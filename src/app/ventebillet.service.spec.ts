import { TestBed } from '@angular/core/testing';

import { VentebilletService } from './ventebillet.service';

describe('VentebilletService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VentebilletService = TestBed.get(VentebilletService);
    expect(service).toBeTruthy();
  });
});
