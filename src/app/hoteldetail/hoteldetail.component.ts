import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service' ;
import {ActivatedRoute} from '@angular/router';
import { Hotel } from '../hotel';

@Component({
  selector: 'app-hoteldetail',
  templateUrl: './hoteldetail.component.html',
  styleUrls: ['./hoteldetail.component.css']
})
export class HoteldetailComponent implements OnInit {
  hotel : any;
  constructor(private hotelserv : HotelService, private route : ActivatedRoute) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.hotelserv.getHotel(id).subscribe(
    hotel =>
      { this.hotel = hotel;
        console.log(this.hotel)
      },
      err => {
        console.log("erreur");
      } 
        );

  }

}
