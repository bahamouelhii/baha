import { Injectable } from '@angular/core';
import { HttpRequest , HttpHandler , HttpEvent , HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class TokeninterceptorService implements HttpInterceptor{

  constructor(private auth : LoginService) { }
  intercept(
    req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
    req = req.clone({ setHeaders: 
      { 'Authorization' : `Bearer ${this.auth.getToken()}` } });
    return next.handle(req);
    }
    

   
  
  }
