import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service' ;

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  client : any ;

  constructor(private clientserv : ClientService) { }

  ngOnInit() {
    this.clientserv.getClients().subscribe(
      resp=>{
        this.client=resp;
        console.log(resp);
      },
      err=>{
        console.log(err);
        
      }
    );
  }


}
