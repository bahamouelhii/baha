import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { LoginService } from './login.service' ;

const url="https://www.topresa.ovh/api/client_indivs.json";


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http : HttpClient , private loginserv : LoginService) { }
  httpOptions = {
    headers : new HttpHeaders ({'Content-Type': 'application/json' , 'Authorization' : `Bearer ${this.loginserv.getToken()}`})
  } ;
  getClients(){
    return this.http.get(url,this.httpOptions);
  }
}
