import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { HotelsComponent } from './hotels/hotels.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ClientComponent } from './client/client.component';
import { VentebilletComponent } from './ventebillet/ventebillet.component';
import { TokeninterceptorService } from './tokeninterceptor.service';
import { CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { HoteldetailComponent } from './hoteldetail/hoteldetail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VoyagesComponent,
    FooterComponent,
    ContactComponent,
    HotelsComponent,
    NavbarComponent,
    LoginComponent,
    ClientComponent,
    VentebilletComponent,
    HoteldetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, DateTimePickerModule
  ],
  providers: [
    {
      provide:
        HTTP_INTERCEPTORS,
        useClass:
          TokeninterceptorService,
          multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
