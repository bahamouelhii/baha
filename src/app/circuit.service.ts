import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const url="https://www.topresa.ovh/api/circuits.json"

@Injectable({
  providedIn: 'root'
})
export class CircuitService {

  constructor(private http : HttpClient) { }
  getCircuits(){
    return this.http.get(url);
  }
}
