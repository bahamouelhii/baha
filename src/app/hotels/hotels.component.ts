import { Component, OnInit } from '@angular/core';
import { Hotel } from '../hotel' ;
import { VilleService } from '../ville.service' ;
import { HotelService } from '../hotel.service' ;
import { CircuitService } from '../circuit.service' ;
import { Datehotel } from '../datehotel';

let hot="Ahmed";

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  hat = new Hotel();
  objet_hotel = new Datehotel();
  hott = hot;
  x = 5;
  y = 10;
  hotels : any;
  hotels2 : any;
  villes : any;
  circuits : any
    public month: number = new Date().getMonth();
    public fullYear: number = new Date().getFullYear();
    public start: Date = new Date(this.fullYear, this.month - 1 , 7);
    public end: Date = new Date(this.fullYear, this.month, 25);
  constructor(private villeserv : VilleService , private hotelserv : HotelService , private circuitserv : CircuitService) { }

  ngOnInit() {
    
    console.log(this.hott)
    this.hat.id=1;
    this.hat.nom="Hambra";
    this.hat.categorie=5;
    this.hat.ville="Yasmine Hammamet";
    this.test();
    this.villeserv.direBonjour();
    this.villeserv.test(hot);
    console.log(this.villeserv.total(this.x,this.y));
    console.log(hot);
    console.log(this.hat);

    this.hotelserv.getHotels().subscribe(
      resp=>{
        this.hotels=resp;
        console.log(resp);
      },
      err=>{
        console.log("Erreur");
      }
    );
    this.villeserv.getVilles().subscribe(
      resp=>{
        this.villes=resp;
        console.log(resp);
      },
      err=>{
        console.log("Erreur");
      }
    );

    this.circuitserv.getCircuits().subscribe(
      resp=>{
        this.circuits=resp;
        console.log(resp);
      },
      err=>{
        console.log("Erreur");
      }
    );
  }

  test(){
    console.log("exemple");
  }

  boutton(){
    console.log(this.start,"  ",this.end)
    this.objet_hotel.date_deb=this.start;
    this.objet_hotel.date_fin=this.end;
    this.hotelserv.getHotelDate(this.objet_hotel).subscribe(
      resp=>{
        this.hotels2=resp;
        console.log(resp);
      },
      err=>{
        console.log("Erreur");
      }
    );
  }

}
