import { Component, OnInit } from '@angular/core';
import { User } from '../user' ;
import { LoginService } from '../login.service' ;



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User() ;
  usert : any;
  constructor(private loginserv : LoginService) { }

  ngOnInit() {
    
  }

  get(){
    console.log(this.user);
    this.loginserv.getUser(this.user).subscribe(
      resp=>{
        this.usert=resp;
        console.log(resp);
        localStorage.setItem('token',this.usert.token);
        console.log(this.loginserv.loggedIn());
      },
      err=>{
        console.log("Erreur");
      }
    );
    
  }
  logout(){
    this.loginserv.logout();
  }

}
