import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { LoginService } from './login.service' ;

const url="https://www.topresa.ovh/api/ventebillets.json";

@Injectable({
  providedIn: 'root'
})
export class VentebilletService {

  constructor(private http : HttpClient , private loginserv : LoginService) { }
  httpOptions = {
    headers : new HttpHeaders ({'Content-Type': 'application/json'})
  } ;
  getBillet(){
    return this.http.get(url,this.httpOptions);
  }

}
