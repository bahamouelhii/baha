import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
const url="https://www.topresa.ovh/api/villes.json";

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  constructor(private http : HttpClient) { }
  getVilles(){
    return this.http.get(url);
  }
  direBonjour(){
    console.log('Bonjour');
  }

  test(nom){
    console.log('bonjour '+nom)
  }

  total(a,b){
    return(a+b);
  }
}
