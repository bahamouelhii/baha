import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hotel } from './hotel';
import { Observable } from 'rxjs';

const url="https://www.topresa.ovh/api/hotels.json";
const url2="https://freedomtravel.tn/ng/hotels.php";
const url3="https://www.topresa.ovh/api/hotels/";

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http : HttpClient) { }

  getHotels(){
    return this.http.get(url);
  }
  getHotel(id):Observable<Hotel>{
    return this.http.get<Hotel>(url3+id);
  }

  getHotelDate(obj){
    return this.http.post(url2,obj);
  }
  
}
