import { Component, OnInit } from '@angular/core';
import { VentebilletService } from '../ventebillet.service' ;

@Component({
  selector: 'app-ventebillet',
  templateUrl: './ventebillet.component.html',
  styleUrls: ['./ventebillet.component.css']
})
export class VentebilletComponent implements OnInit {

  constructor(private ventebilletserv : VentebilletService) { }

  ngOnInit() {
    this.ventebilletserv.getBillet().subscribe(
      resp=>{
        console.log(resp);
      },
      err=>{
        console.log(err);

        
      }
    );
  }

}
