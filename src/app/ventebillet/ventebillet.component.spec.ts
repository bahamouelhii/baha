import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentebilletComponent } from './ventebillet.component';

describe('VentebilletComponent', () => {
  let component: VentebilletComponent;
  let fixture: ComponentFixture<VentebilletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentebilletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentebilletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
