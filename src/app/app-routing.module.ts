import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HotelsComponent } from './hotels/hotels.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { HoteldetailComponent } from './hoteldetail/hoteldetail.component';
import { VentebilletComponent } from './ventebillet/ventebillet.component';
import { AuthGuard } from './auth.guard' ;

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'hotels', component: HotelsComponent },
  { path: 'voyages', component: VoyagesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'client', component: ClientComponent },
  { path: 'hoteldetail/:id', component: HoteldetailComponent },
  { path: 'ventebillet', component: VentebilletComponent, canActivate: [AuthGuard]},
  { path: '**', component: HomeComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
