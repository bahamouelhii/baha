import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
const url="https://www.topresa.ovh/api/login_check";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http : HttpClient, private router : Router) { }
  getUser(user){
    return this.http.post(url,user);
  }
  loggedIn(){
    return !!this.getToken() ;
  }
  getToken(){
    return localStorage.getItem('token');
  }
  logout(){
    localStorage.removeItem('token');
    console.log("loggued out")
    this.router.navigate(['home']);
  }
}
